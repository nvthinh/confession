# This file is used by Rack-based servers to start the application.
require 'bundler/setup'
require 'omniauth-facebook'

use Rack::Session::Cookie

use OmniAuth::Builder do
    provider :facebook, "592878537575163", "303845d54be046d77bef20ef28d8a2cc"
    #provider :facebook, ENV['592878537575163'], ENV['303845d54be046d77bef20ef28d8a2cc']
end

require ::File.expand_path('../config/environment', __FILE__)
run Rails.application
